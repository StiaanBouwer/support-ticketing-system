<?php

namespace App\Http\Controllers;

use App\Mail\TicketSubmitted;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        if(Auth::user()->isAdmin()) {
            $tickets = Ticket::all();
        } else {
            $tickets = Ticket::where('user_id', Auth::id())->get();
        }

        return view('dashboard', [
            'tickets' => $tickets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('ticket/create');
    }

    /**
     * Show the form for adding contact details to a ticket.
     */
    public function contactDetails($ticketId)
    {
        return view('ticket/contact-details', ['ticketId' => $ticketId]);
    }

    public function storeContactDetails(Request $request, $ticketId)
    {
        $this->validate($request, [
            'email' => 'required|email|max:64',
            'mobile' => 'nullable|min:8|max:16'
        ]);

        $ticket = Ticket::findOrFail($ticketId);
        $ticket->update([
            'email' => $request->email,
            'mobile' => $request->mobile
        ]);

        return redirect(route('ticket.logged', [$ticketId]));
    }

    /**
     * Show a message indicating that a ticket has been logged.
     */
    public function logged($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);
        Mail::to($ticket->email)->send(new TicketSubmitted($ticketId));

        return view('ticket/ticket-logged', ['ticketId' => $ticketId]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|max:225',
            'type' => 'required|integer',
        ]);

        $ticket = Ticket::create([
            'user_id' => Auth::id(),
            'description' => $request->description,
            'type' => $request->type,
        ]);

        return redirect(route('ticket.contact-details', $ticket->id));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $ticket = Ticket::find($id);

        return view('ticket.show', ['ticket' => $ticket]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
