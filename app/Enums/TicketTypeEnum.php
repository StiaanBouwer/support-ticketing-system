<?php

namespace App\Enums;

enum TicketTypeEnum:int {
    case Sales = 1;
    case Accounts = 2;
    case IT = 3;
}
