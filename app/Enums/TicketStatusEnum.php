<?php

namespace App\Enums;

enum TicketStatusEnum:int {
    case Newly_Logged = 1;
    case In_Progress = 2;
    case Resolved = 3;
}
