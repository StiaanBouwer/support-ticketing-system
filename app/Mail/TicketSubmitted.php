<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TicketSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $ticketId;

    /**
     * Create a new message instance.
     */
    public function __construct($ticketId)
    {
        $this->ticketId = $ticketId;
    }

    public function build()
    {
        return $this->subject('Support Ticket Received')
            ->view('emails.ticket-submitted', ['ticketId' => $this->ticketId]);
    }
}
