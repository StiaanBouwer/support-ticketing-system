<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class TicketTable extends Component
{
    use WithPagination;

    public $tickets;

    public function mount($tickets)
    {
        $this->tickets = $tickets;
    }

    public function render()
    {
        return view('livewire.ticket-table');
    }
}
