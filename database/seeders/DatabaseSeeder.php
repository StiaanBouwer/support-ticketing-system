<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Enums\RoleEnum;
use App\Enums\TicketStatusEnum;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Create admin user
        User::create([
            'name' => 'John',
            'surname' => 'Doe',
            'email' => 'admin@support-ticket.com',
            'email_verified_at' => now(),
            'role' => RoleEnum::Admin,
            'password' => Hash::make('password'),
        ]);

        // Create normal user
        User::create([
            'name' => 'Harry',
            'surname' => 'Henderson',
            'email' => 'user1@support-ticket.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
        ]);

        // Create normal user
        User::create([
            'name' => 'Harry',
            'surname' => 'Cane',
            'email' => 'user2@support-ticket.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
        ]);

        // Create normal user
        User::create([
            'name' => 'Alice',
            'surname' => 'Frank',
            'email' => 'user3@support-ticket.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
        ]);

        Ticket::create([
            'user_id' => 2,
            'title' => 'Test Ticket #1',
            'description' => 'Test Description',
            'status' => TicketStatusEnum::Newly_Logged
        ]);
        Ticket::create([
            'user_id' => 2,
            'title' => 'Test Ticket #2',
            'description' => 'Test Description',
            'status' => TicketStatusEnum::In_Progress
        ]);
        Ticket::create([
            'user_id' => 2,
            'title' => 'Test Ticket #3',
            'description' => 'Test Description',
            'status' => TicketStatusEnum::Resolved
        ]);
    }
}
