# Supporting Ticketing System
For a more detailed dev environment setup, see README.md

### On First run ever
We need to build the docker image first (This only happens once unless the `Docker` file has been modified)

### Running the containers

Run the following commands to start the projects containers then navigate to `localhost`

Just add the `-d` flag

```shell
./vendor/bin/sail up -d
```

### Stopping your containers

```shell
./vendor/bin/sail down
```

### Migrate database tables and run the database seeders


```shell
./vendor/bin/sail artisan migrate:fresh --seed
```

## NPM
Next compile your front end with vite using NPM

```shell
./vendor/bin/sail npm install 
./vendor/bin/sail npm run build
```

Optional

### Mail
Go to localhost:8025 to see your test emails.

### Admin Logins
```shell
email :     admin@support-ticket.com
password :  password
```

### User Logins
```shell
email :     user1@support-ticket.com
password :  password
```
```shell
email :     user2@support-ticket.com
password :  password
```
```shell
email :     user3@support-ticket.com
password :  password
```
