@php use App\Enums\TicketStatusEnum;use App\Enums\TicketTypeEnum; @endphp
<div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Type</th>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
            @foreach($tickets as $ticket)
                <tr>
                    <th scope="row">{{$ticket->id}}</th>
                    <td>
                        @if($ticket->type == TicketTypeEnum::Sales->value)
                            {{TicketTypeEnum::Sales->name}}
                        @elseif($ticket->type == TicketTypeEnum::Accounts->value)
                            {{TicketTypeEnum::Accounts->name}}
                        @elseif($ticket->type == TicketTypeEnum::IT->value)
                            {{TicketTypeEnum::IT->name}}
                        @endif
                    </td>
                    <td>{{$ticket->user->name}}</td>
                    <td>{{$ticket->user->surname}}</td>
                    <td>
                        @if($ticket->status == TicketStatusEnum::Newly_Logged)
                            Newly Logged
                        @elseif($ticket->status == TicketStatusEnum::In_Progress)
                            In Progress
                        @elseif($ticket->status == TicketStatusEnum::Resolved)
                            Resolved
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
