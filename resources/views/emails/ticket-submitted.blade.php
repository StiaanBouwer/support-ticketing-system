{{ __('Your ticket has been received') }}
<br>
{{ __('One of our agents will be in contact with you soon.') }}
<br>
<a href="{{ route('ticket.show', $ticketId) }}">{{ __('View Status') }}</a>
