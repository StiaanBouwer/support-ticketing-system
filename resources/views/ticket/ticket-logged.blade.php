<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Log a Ticket') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg p-4">
                {{--Breadcrumb--}}
                <div class="container mb-4">
                    <div class="row">
                        <ul class="breadcrumb">
                            <li class="completed"><a href="javascript:void(0);">Log a Ticket</a></li>
                            <li class="completed"><a href="javascript:void(0);">Contact Details</a></li>
                            <li class="completed"><a href="javascript:void(0);">Done!</a></li>
                        </ul>
                    </div>
                </div>
                <form action="{{ route('ticket.show', $ticketId) }}">
                    <p class="font-semibold">Your ticket has been submitted!</p>
                    <p class="text-muted">Click on the button below to view the status.</p>
                    <button type="submit" class="btn btn-primary float-right">View Status</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
