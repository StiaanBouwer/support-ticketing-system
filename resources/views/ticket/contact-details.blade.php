<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Log a Ticket') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg p-4">
                {{--Breadcrumb--}}
                <div class="container mb-4">
                    <div class="row">
                        <ul class="breadcrumb">
                            <li class="completed"><a href="javascript:void(0);">Log a Ticket</a></li>
                            <li class="completed"><a href="javascript:void(0);">Contact Details</a></li>
                            <li class="active"><a href="javascript:void(0);">Done!</a></li>
                        </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('ticket.store-contact-details', $ticketId) }}">
                    @csrf
                    {{--Email--}}
                    <div class="form-group my-4">
                        <label for="email">Email Address</label>
                        <input required name="email" type="email" class="form-control" id="email" aria-describedby="email" placeholder="Enter email" value="{{ old('email') }}">
                        @error('email')
                            <div class="alert alert-danger my-3">{{ $message }}</div>
                        @enderror
                    </div>
                    {{--Number--}}
                    <div class="form-group my-4">
                        <label for="mobile">Contact Number</label>
                        <input name="mobile" type="text" class="form-control" id="mobile" aria-describedby="mobile" placeholder="Enter number" value="{{ old('mobile') }}">
                        @error('mobile')
                            <div class="alert alert-danger my-3">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                    <p id="emailHelp" class="form-text text-muted my-2 float-right mx-4">Your information will never be shared with anyone else.</p>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
