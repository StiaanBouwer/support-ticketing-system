@php use App\Enums\TicketTypeEnum; @endphp
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Log a Ticket') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg p-4">
                {{--Breadcrumb--}}
                <div class="container mb-4">
                    <div class="row">
                        <ul class="breadcrumb">
                            <li class="completed"><a href="javascript:void(0);">Log a Ticket</a></li>
                            <li class="active"><a href="javascript:void(0);">Contact Details</a></li>
                            <li class="active"><a href="javascript:void(0);">Done!</a></li>
                        </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('ticket.store') }}">
                    @csrf
                    {{--Ticket Type--}}
                    <div class="form-check form-check-inline">
                        <input required name="type" class="form-check-input" type="radio" id="type-sales" value="{{ TicketTypeEnum::Sales->value }}" {{ old('type') == TicketTypeEnum::Sales->value ? 'checked' : '' }}>
                        <label class="form-check-label" for="type-sales">Sales</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input required name="type" class="form-check-input" type="radio" id="type-accounts" value="{{ TicketTypeEnum::Accounts->value }}" {{ old('type') == TicketTypeEnum::Accounts->value ? 'checked' : '' }}>
                        <label class="form-check-label" for="type-accounts">Accounts</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input required name="type" class="form-check-input" type="radio" id="type-it" value="{{ TicketTypeEnum::IT->value }}" {{ old('type') == TicketTypeEnum::IT->value ? 'checked' : '' }}>
                        <label class="form-check-label" for="type-it">IT</label>
                    </div>
                    @error('type')
                        <div class="alert alert-danger my-3">{{ $message }}</div>
                    @enderror
                    {{--Message--}}
                    <div class="form-group my-2">
                        <textarea required name="description" class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="Describe you issue to the best of your ability.">{{ old('description') }}</textarea>
                        @error('description')
                            <div class="alert alert-danger my-3">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary my-3 float-right">Next</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
