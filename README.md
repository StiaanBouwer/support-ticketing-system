# Supporting Ticketing System
For a quick setup, see QUICKSTART.md

This setup simulates a git repository that have been cloned.

## Clone the Repository
To clone the repository from git, run 
```shell 
git clone git@gitlab.com:StiaanBouwer/support-ticketing-system.git
```


## Setup
### Dot Environment
This file (.env) is your environment file. Laravel and other tools in the project make use of this file for environmental variables that are locally defined for your development environment.

First open the .env file, if it doesn't exist either copy the .env.example file and rename it .env or run the command below

```shell
cp .env.example .env
```

```DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=user
DB_PASSWORD=password
```

### PORT's
This will change the port of the laravel application. By default, laravel will be accessible at localhost:8000.
We can control the port number with the following variable

```dotenv
APP_PORT=80
DB_PORT=3306
```

### Sail
To add the `sail` alias to your system:

```shell
nano ~/.bashrc
```

Add the alias in the bashrc file:
```shell
alias sail="./vendor/bin/sail"
```

Then activate by typing the following in your terminal:
```shell
source ~/.bashrc
```

if sail command doesnt work add:
Then activate by typing the following in your terminal:
```shell
(alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail')
```

if you get the error:
```shell
bash: ./vendor/bin/sail: No such file or directory
```
run composer install in the project folder to generate the vendor folder:
```shell
composer install && composer update
```

## Starting the containers | Sail

*If you get a permission denied or the daemon could not run / be found you didn't add your user to the docker/sail group*

Run the following commands to start the projects containers then navigate to `localhost:<APP_PORT>`
#### With logs

```shell
sail up
```

#### Without logs
Just add the `-d` flag

```shell
sail up -d
```

#### Run this before continuing with other sail processes
```shell
sail artisan optimize:clear
```

#### Stopping your containers

```shell
sail down
```

## Dependencies

Next we need to execute some commands in the container run the following in the app container

### Composer
Composer is PHP's dependency manager. We need to run the following in the app container to install our dependencies

```shell
sail composer install
```

and to update them


```shell
sail composer update
```

### Generate the app key

To generate the key run

```shell
sail artisan key:generate
```

### Migrate database tables and run the database seeders


```shell
sail artisan migrate:fresh --seed
```

### Clear the cache config

```shell
sail artisan config:cache
```

## NPM

Next compile your front end with vite using NPM

```shell
sail npm install 
sail npm run build
```

Optionally, you can also compile the JavaScript and Front-End as you develop using the following:

```shell
sail npm run dev
```

### Mail
Head over to localhost:8025 to see your test emails.

## First login

### Admin Logins
```shell
email :     admin@support-ticket.com
password :  password
```

### User Logins
```shell
email :     user1@support-ticket.com
password :  password
```
```shell
email :     user2@support-ticket.com
password :  password
```
```shell
email :     user3@support-ticket.com
password :  password
```

## Possible Issues
* If you receive the error `-bash: vendor/bin/sail: No such file or directory`
    * If you receive this error while installing dependencies, ensure that the containers are running.
    * Ensure that the sail file is found inside the `vendor/bin/` folder.
    * If the file is present, try deleting the vendor folder and pulling the sail packages from git again.
