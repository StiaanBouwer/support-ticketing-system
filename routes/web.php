<?php

use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    if(Auth::check()) {
        return redirect(route('dashboard'));
    }
    return view('home');
})->name('home');

// Anonymously view a ticket's status
Route::get('/ticket/show/{id}', [TicketController::class, 'show'])->name('ticket.show');

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified',])->group(function () {

    // User Dashboard
    Route::get('/dashboard', [TicketController::class, 'index'])->name('dashboard');

    // Create Ticket Journey
    Route::get('/ticket/create', [TicketController::class, 'create'])->name('ticket.create');
    Route::post('/ticket/store', [TicketController::class, 'store'])->name('ticket.store');
    Route::get('/ticket/details/{id}', [TicketController::class, 'contactDetails'])->name('ticket.contact-details');
    Route::post('/ticket/store-contact-details/{id}', [TicketController::class, 'storeContactDetails'])->name('ticket.store-contact-details');
    Route::get('/ticket/logged/{id}', [TicketController::class, 'logged'])->name('ticket.logged');

    // Admin routes
    Route::middleware('admin')->group(function () {

    });
});
